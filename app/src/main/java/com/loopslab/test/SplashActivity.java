package com.loopslab.test;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;

import java.util.Timer;
import java.util.TimerTask;

public class SplashActivity extends AppCompatActivity {
    private static final int mSplashTime = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        RotateAnimation anim = new RotateAnimation(0, 180,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        anim.setInterpolator(new LinearInterpolator());
        anim.setRepeatCount(Animation.INFINITE);
        anim.setDuration(4000);
        final ImageView rotate_version_number = (ImageView) findViewById(R.id.rotate_version_number);
        rotate_version_number.startAnimation(anim);

      /*  TranslateAnimation backAnim = new TranslateAnimation(
                 0f,
                 0f,
                   -5f,
                    -50f);
        backAnim.setDuration(1000);
        backAnim.setInterpolator(new BounceInterpolator());
        backAnim.setRepeatCount(-1);


        final  ImageView splash_background = (ImageView) findViewById(R.id.splash_background);
        splash_background.startAnimation(backAnim);

        //splash_background = BitmapFactory.decodeResource(SplashActivity.this.getResources(),
               // R.drawable.splash_background,options);

        //final SurfaceView surfaceView = (SurfaceView) findViewById(R.id.splash_background);
        //  splash_background.startAnimation(backAnim);
        */

        Timer mTimer = new Timer();
        mTimer.schedule(
                new TimerTask() {
                    public void run() {
                        finish();
                        Intent intent = new Intent(SplashActivity.this, DetailActivity.class);
                        startActivity(intent);
                    }
                },
                mSplashTime);

    }


}
